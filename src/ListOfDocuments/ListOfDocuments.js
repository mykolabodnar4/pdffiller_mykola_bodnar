import {Button} from "../Button/Button";
import { ReactComponent as DocumentIcon } from "../icons/DocumentIcon.svg";
import './ListOfDocuments.scss';


export function ListOfDocuments(props) {
    const content = props.documents.map((documents) =>
        <div key={documents.id} className="ListOfDocumentItem">
            <div className="DocumentInformation">
                <div className="DocumentIcon">
                    <DocumentIcon/>
                </div>
                <div className="DocumentInfo">
                    <p className="DocumentTitle">{documents.title}</p>
                    <p className="DocumentLastEdit">{documents.content}</p>
                </div>
            </div>
            <Button/>
        </div>
    );
    return (
        <div className="ListOfDocument">
            {content}
        </div>
    );
}
