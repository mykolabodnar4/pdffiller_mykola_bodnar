import './Button.scss';

export function Button() {
    return (
        <>
            <button className="Button">
              <p className="ButtonText">
                  View documents
              </p>
            </button>
        </>
    );
}
