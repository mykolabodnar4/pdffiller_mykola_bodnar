import './App.css';
import {ListOfDocuments} from "./ListOfDocuments/ListOfDocuments";

const documents = [
    {id: 1, title: 'Rundofase', content: 'Last Edited: 08/08/2020'},
    {id: 2, title: 'Genco Pura Olive Oil Company', content: 'Last Edited: 08/08/2020'},
    {id: 3, title: 'Bubba Gump', content: 'Last Edited: 08/08/2020'}
];

function App() {
    return (
        <>
          <ListOfDocuments documents={documents}/>
        </>
    );
}

export default App;

